# Demo iGenius

## Component libraries

* [Alamofire](https://github.com/Alamofire/Alamofire)
* [AlamofireImage](https://github.com/Alamofire/AlamofireImage)
* [IGListKit](https://github.com/Instagram/IGListKit)
* [Hero](https://github.com/HeroTransitions/Hero)

## Configuration

In the Info.plist file fill each empty item under the **Instagram** key with your Instagram API client data (e.g. ClientID, ClientSecret, RedirectURL).

## Missing features

At the moment the app doesn't support offline usage.

## Usage tips

Swipe down to dismiss fullscreen gallery.
