//
//  Comment.swift
//  DemoiGenius
//
//  Created by Omar Carrassi on 09/12/2018.
//  Copyright © 2018 Omar Carrassi. All rights reserved.
//

import Foundation

public struct Comment: Decodable {
    
    // MARK: - PROPERTIES
    
    public let id: String
    public let text: String
    public let createdDate: Date
    
    // MARK: - TYPES
    
    private enum CodingKeys: String, CodingKey {
        case id, text, createdTime = "created_time"
    }
    
    // MARK: - LIFE CYCLE METHODS
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(String.self, forKey: .id)
        text = try container.decode(String.self, forKey: .text)
        let createdTime = try container.decode(String.self, forKey: .createdTime)
        createdDate = Date(timeIntervalSince1970: Double(createdTime)!)
    }
}
