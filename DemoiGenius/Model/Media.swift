//
//  Media.swift
//  DemoiGenius
//
//  Created by Omar Carrassi on 09/12/2018.
//  Copyright © 2018 Omar Carrassi. All rights reserved.
//

import Foundation

public struct Media: Decodable {
    
    // MARK: - PROPERTIES
    
    public let id: String
    public let createdDate: Date
    public let type: String
    public let images: Images
    public let videos: Videos?
    public let caption: Comment?
    public let comments: Count
    public let likes: Count
    public let link: URL
    public let carouselMedia: [CarouselMedia]?
    
    
    // MARK: - TYPES
    
    // A struct cointaing the number of elements.
    public struct Count: Decodable {
        
        public let count: Int
    }
    
    // A struct containing the resolution of a video or image.
    public struct Resolution: Decodable {
        
        public let width: Int
        public let height: Int
        public let url: URL
    }
    
    // A struct cointaining the thumbnail, low and high resolution images of the media.
    public struct Images: Decodable {
        
        public let thumbnail: Resolution
        public let low_resolution: Resolution
        public let standard_resolution: Resolution
    }
    
    // A struct cointaining the low and standard resolution videos of the media.
    public struct Videos: Decodable {
        
        public let low_resolution: Resolution
        public let standard_resolution: Resolution
        public let low_bandwidth: Resolution?
    }
    
    // The struct containing the images or videos of the carousel.
    public struct CarouselMedia: Decodable {
        
        public let images: Images?
        public let videos: Videos?
        public let type: String
    }
    
    private enum CodingKeys: String, CodingKey {
        case id, user, createdTime = "created_time", type, images, videos, caption, comments, likes, link, carouselMedia
    }
    
    // MARK: - LIFE CYCLE METHODS
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(String.self, forKey: .id)
        let createdTime = try container.decode(String.self, forKey: .createdTime)
        createdDate = Date(timeIntervalSince1970: Double(createdTime)!)
        type = try container.decode(String.self, forKey: .type)
        images = try container.decode(Images.self, forKey: .images)
        videos = try container.decodeIfPresent(Videos.self, forKey: .videos)
        caption = try container.decode(Comment.self, forKey: .caption)
        comments = try container.decode(Count.self, forKey: .comments)
        likes = try container.decode(Count.self, forKey: .likes)
        link = try container.decode(URL.self, forKey: .link)
        carouselMedia = try container.decodeIfPresent([CarouselMedia].self, forKey: .carouselMedia)
    }
}
