//
//  UserModelController.swift
//  DemoiGenius
//
//  Created by Omar Carrassi on 05/12/18.
//  Copyright © 2018 Omar Carrassi. All rights reserved.
//

import Foundation

class UserModelController {
    
    // MARK: - API DELEGATE TYPES
    
    typealias OperationCompletedWithResult = (_ success: Bool, _ error: Instagram.error?) -> Void
    typealias OperationCompletedWithResultAndArrayData = (_ success: Bool, _ error: Error?, _ data: [AnyObject]) -> Void
    
    
    // MARK: - PROPERTIES
    
    private let _userDefaults: UserDefaults
    private let _apiManager: APIManager
    private let _keychain = KeychainSwift(keyPrefix: "DemoiGenius_")
    private let _instagram: Instagram
    
    private var _currentUser: User?
    
    var currentUser: User? {
        get {
            return _currentUser
        }
    }
    
    var userLoggedIn : Bool {
        return retrieveAccessToken() != nil
    }
    
    private enum StorageKeys {
        static let accessTokenKey = "access_token"
        static let currentUserKey = "current_user"
    }
    
    
    // MARK: - USER LIFE CYCLE METHODS
    
    init(userDefaults: UserDefaults = .standard, apiManager: APIManager = .sharedInstance)
    {
        // Inject required singletons
        _userDefaults = userDefaults
        _apiManager = apiManager
        
        // Instantiate Instagram object using plist data
        do {
            if  let instagramPlistData = Bundle.main.object(forInfoDictionaryKey: "Instagram") as? [String:String] {
                let jsonData = try JSONSerialization.data(withJSONObject: instagramPlistData, options: .prettyPrinted)
                _instagram = try JSONDecoder().decode(Instagram.self, from: jsonData)
            } else {
                fatalError("Missing Instagram config in Info.plist")
            }
        } catch _ {
            fatalError("Missing Instagram config in Info.plist")
        }
        
        // Read stored data
        if let user = loadUserData() {
            _currentUser = user
        }
    }
    
    
    // MARK: - USER LIFE CYCLE METHODS
    
    private func updateCurrentUser(withData data: Dictionary<String, AnyObject>)
    {
        if  let id = data["id"] as? String,
            let username = data["username"] as? String,
            let profilePicUrl = data["profile_picture"] as? String,
            let fullName = data["full_name"] as? String,
            let bio = data["bio"] as? String
        {
            guard let id = Int(id) else { return }
            _currentUser = User(id: id, username: username, profilePicUrl: profilePicUrl, fullName: fullName, bio: bio)
            storeUserData()
        }
    }
    
    
    // MARK: - AUTH METHODS
    
    func logUserIn(withAuthCode authCode: String, completion: @escaping OperationCompletedWithResult)
    {
        // Check if a code has been passed
        guard authCode != "" else {
            completion(false, Instagram.error.emptyAuthCode)
            return
        }
        
        // Params for Instagram's access token API request
        let params = [
            "client_id": _instagram.clientID,
            "client_secret": _instagram.clientSecret,
            "grant_type": _instagram.grantType,
            "redirect_uri": _instagram.redirectURL,
            "code": authCode
        ]
        
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        
        // Load access token request
        _apiManager.performRequest(withUrl: _instagram.accessTokenURL,
                                   method: "post",
                                   parameters: params as [String : AnyObject],
                                   headers: headers,
                                   usingToken: "")
        { (success, error, data) -> Void in
            
            if success == true {
                // Update current user data
                if let userData = data["user"] as? Dictionary<String, AnyObject> {
                    self.updateCurrentUser(withData: userData)
                }
                
                // Update access token
                if let accessToken = data["access_token"] as? String {
                    guard self.storeAccessToken(accessToken) else {
                        completion(false, Instagram.error.keychainError(code: self._keychain.lastResultCode))
                        return
                    }
                    completion(true, nil)
                } else {
                    if let responseCode = data["code"] as? Int, responseCode == 400 {
                        completion(false, Instagram.error.authException)
                    } else {
                        completion(false, Instagram.error.invalidResponse)
                    }
                }
            } else {
                completion(false, Instagram.error.invalidRequest)
            }
            
        }
    }
    
    func logUserOut() -> Bool {
        _currentUser = nil
        return (deleteUserData() && deleteAccessToken())
    }
    
    
    // MARK: - USER DATA FETCHING METHODS
    
    func fetchUserProfile(completion: @escaping OperationCompletedWithResult)
    {
        // Retrieve token from keychain
        guard let accessToken = retrieveAccessToken() else {
            completion(false, Instagram.error.authException)
            return
        }
        
        // Load access token request
        _apiManager.performRequest(withUrl: _instagram.currentUserApiURL,
                                   method: "get",
                                   usingToken: accessToken)
        { (success, error, data) -> Void in
            
            if success == true {
                if let responseCode = data["meta"]?["code"] as? Int, responseCode == 400 {
                    completion(false, Instagram.error.authException)
                    return
                }
                
                // Update current user data
                if let userData = data["data"] as? Dictionary<String, AnyObject> {
                    self.updateCurrentUser(withData: userData)
                    completion(true, nil)
                }
            } else {
                completion(false, Instagram.error.invalidRequest)
            }
            
        }
    }
    
    func fetchUserMedia(completion: @escaping OperationCompletedWithResultAndArrayData)
    {
        // Retrieve token from keychain
        guard let accessToken = retrieveAccessToken() else {
            completion(false, Instagram.error.authException, [])
            return
        }
        
        // Load access token request
        _apiManager.performRequest(withUrl: _instagram.mediaApiURL,
                                   method: "get",
                                   usingToken: accessToken)
        { (success, error, data) -> Void in
            
            if success == true {
                if let responseCode = data["meta"]?["code"] as? Int, responseCode == 400 {
                    completion(false, Instagram.error.authException, [])
                    return
                }
                
                do {
                    guard let responseData = data["data"] as? [[String:AnyObject]] else {
                        completion(false, Instagram.error.invalidResponse, [])
                        return
                    }
                    
                    // Return fetched media
                    var mediaArray: [Media] = []
                    for mediaObj in responseData {
                        // Convert dictionary of media object to raw data
                        let mediaData = try JSONSerialization.data(withJSONObject: mediaObj, options: .prettyPrinted)
                        // Convert media object data to string
                        if let mediaString = String(data: mediaData, encoding: .utf8) {
                            // Get json raw data from media object string
                            let jsonData = mediaString.data(using: .utf8)!
                            // Init new Media instance from json raw data
                            let media = try JSONDecoder().decode(Media.self, from: jsonData)
                            // Add new media instance to array
                            mediaArray.append(media)
                        }
                    }
                    // Sort array on media creation date (desc order)
                    mediaArray.sort(by: { (media1, media2) -> Bool in
                        return media1.createdDate.compare(media2.createdDate) == .orderedDescending
                    })
                    
                    completion(true, nil, mediaArray as [AnyObject])
                } catch _ {
                    completion(false, Instagram.error.invalidResponse, [])
                    return
                }
            } else {
                completion(false, Instagram.error.invalidRequest, [])
            }
            
        }
    }
    
    
    // MARK: - UTILS
    
    // Load stored current user's data from UserDefaults
    private func loadUserData() -> User?
    {
        if let userData = _userDefaults.data(forKey: StorageKeys.currentUserKey) {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? User {
                return user
            }
        }
        
        return nil
    }
    
    // Store current user's data to UserDefaults
    private func storeUserData() -> Bool
    {
        if let user = _currentUser {
            let userData = NSKeyedArchiver.archivedData(withRootObject: user)
            _userDefaults.set(userData, forKey: StorageKeys.currentUserKey)
            return _userDefaults.synchronize()
        } else {
            return false
        }
    }
    
    // Deletes current user's data from UserDefaults
    private func deleteUserData() -> Bool
    {
        _userDefaults.removeObject(forKey: StorageKeys.currentUserKey)
        return _userDefaults.synchronize()
    }
    
    // Store your own authenticated access token so you don't have to use the included login authentication.
    private func storeAccessToken(_ accessToken: String) -> Bool
    {
        return _keychain.set(accessToken, forKey: StorageKeys.accessTokenKey)
    }
    
    // Returns the current access token.
    private func retrieveAccessToken() -> String?
    {
        return _keychain.get(StorageKeys.accessTokenKey)
    }
    
    // Deletes the current access token
    private func deleteAccessToken() -> Bool
    {
        return _keychain.delete(StorageKeys.accessTokenKey)
    }
    
}
