//
//  User.swift
//  DemoiGenius
//
//  Created by Omar Carrassi on 04/12/2018.
//  Copyright © 2018 Omar Carrassi. All rights reserved.
//

import Foundation

class User: NSObject, NSCoding {
    
    // MARK: - PROPERTIES
    
    let id: Int
    let username: String
    let profilePicUrl: String
    let fullName: String
    let bio: String
    
    
    // MARK: - INIT METHODS
    
    init(id: Int, username: String, profilePicUrl: String, fullName: String, bio: String) {
        self.id = id
        self.username = username
        self.profilePicUrl = profilePicUrl
        self.fullName = fullName
        self.bio = bio
        
        super.init()
    }
    
    
    // MARK: - NSCODING METHODS
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init(
            id: aDecoder.decodeInteger(forKey: "id"),
            username: (aDecoder.decodeObject(forKey: "username") as? String)!,
            profilePicUrl: (aDecoder.decodeObject(forKey: "profilePicUrl") as? String)!,
            fullName: (aDecoder.decodeObject(forKey: "fullName") as? String)!,
            bio: (aDecoder.decodeObject(forKey: "bio") as? String)!
        )
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(username, forKey: "username")
        aCoder.encode(profilePicUrl, forKey: "profilePicUrl")
        aCoder.encode(fullName, forKey: "fullName")
        aCoder.encode(bio, forKey: "bio")
    }
    
}
