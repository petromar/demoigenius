//
//  CollectionSectionItem.swift
//  DemoiGenius
//
//  Created by Omar Carrassi on 08/12/2018.
//  Copyright © 2018 Omar Carrassi. All rights reserved.
//

import IGListKit
import UIKit

final class CollectionSectionItem: NSObject {
    
    // MARK: - PROPERTIES
    
    var user: User?
    var items: [Media] = []
    
    
    // MARK: - LIFE CYCLE METHODS
    
}

extension CollectionSectionItem: ListDiffable {
    
    func diffIdentifier() -> NSObjectProtocol
    {
        return self
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool
    {
        return self === object ? true : self.isEqual(object)
    }
    
}
