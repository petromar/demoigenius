//
//  APIManager.swift
//  DemoiGenius
//
//  Created by Omar Carrassi on 04/12/2018.
//  Copyright © 2018 Omar Carrassi. All rights reserved.
//

import Alamofire
import Foundation

class APIManager: NSObject {
    
    // MARK: - API DELEGATE TYPES
    
    typealias OperationCompletedWithResultAndDictionaryData = (_ success: Bool, _ error: Error?, _ data: [String: AnyObject]) -> Void
    

    // MARK: - PROPERTIES
    
    private let reachabilityManager: NetworkReachabilityManager
    

    // MARK: - SHARED INSTANCE INIT
    
    override init() {
        
        // Retrieve reachability host from Info.plist
        var reachabilityHost = "https://google.com"
        if  let instagramPlistDict = Bundle.main.object(forInfoDictionaryKey: "Instagram") as? [String:String] {
            if let host = instagramPlistDict["ReachabilityHost"] {
                reachabilityHost = host
            } else {
                fatalError("Missing Instagram config in Info.plist")
            }
        } else {
            fatalError("Missing Instagram config in Info.plist")
        }
        
        // Init network reachability manager to send network related event notifications
        reachabilityManager = NetworkReachabilityManager(host: reachabilityHost)!
        reachabilityManager.listener = { status in
            switch status {
            case .reachable(.ethernetOrWiFi), .reachable(.wwan):
                NotificationCenter.default.post(name: .networkReachable, object: nil)
            default:
                break
            }
        }
        reachabilityManager.startListening()
        
        super.init()
    }
    
    class var sharedInstance: APIManager
    {
        struct Singleton {
            static let instance = APIManager()
        }
        
        return Singleton.instance
    }
    

    // MARK: - API METHODS
    
    func performRequest(withUrl url: String,
                        method: String = "post",
                        parameters: [String: AnyObject] = [:],
                        headers: [String:String] = [:],
                        usingToken token: String = "",
                        completion: @escaping OperationCompletedWithResultAndDictionaryData) {
        
        // Check provided url
        guard url != "" else {
            completion(false, nil, [:])
            return
        }
        
        // Final URL configuration
        var urlComponents = URLComponents(string: url)
        urlComponents?.queryItems = [URLQueryItem]()
        
        // Add access token param to request url
        if token != "" {
            let queryItem = URLQueryItem(name: "access_token", value: token)
            urlComponents?.queryItems?.append(queryItem)
        }
        
        // Add each param to GET request url
        if stringToHTTPMethod(method: method) == .get {
            for param in parameters {
                guard let value = param.value as? String else { continue }
                urlComponents?.queryItems?.append(URLQueryItem(name: param.key, value: value))
            }
        }
        
        // Final URL string
        guard let requestUrl = urlComponents?.url else {
            completion(false, nil, [:])
            return
        }
        
        // Request headers
        let headers = ["Accept": "application/json"].merged(another: headers)
        
        // Start an Alamofire POST request providing parameters and headers
        Alamofire.request(requestUrl, method: stringToHTTPMethod(method: method), parameters: parameters, headers: headers)
            .validate()
            .responseJSON { response in
                print(response)
                
                // Check for response status
                switch response.result {
                case .success:
                    // Get the response value as a Dictionary
                    guard let responseValue = response.result.value as? Dictionary<String, AnyObject> else {
                        completion(false, nil, [:])
                        return
                    }
                    completion(true, nil, responseValue)
                case let .failure(error):
                    var errorResponse: Dictionary<String, AnyObject> = [:]
                    if let responseValue = response.result.value as? Dictionary<String, AnyObject> {
                        errorResponse = responseValue
                    }
                    completion(false, error, errorResponse)
                }
        }
    }
    

    // MARK: - UTILS
    
    /*
     * Returns the HTTPMethod object related to the provided string
     */
    func stringToHTTPMethod(method: String) -> HTTPMethod {
        switch method.lowercased() {
        case "get":
            return .get
        case "post":
            return .post
        case "put":
            return .put
        case "patch":
            return .patch
        case "delete":
            return .delete
        default:
            return .get
        }
    }
    
}
