//
//  Notification+CustomNames.swift
//  DemoiGenius
//
//  Created by Omar Carrassi on 07/12/18.
//  Copyright © 2018 Omar Carrassi. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let networkReachable = Notification.Name("networkReachable")
}
