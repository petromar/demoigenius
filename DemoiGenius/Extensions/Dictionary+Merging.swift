//
//  Dictionary+Merging.swift
//  DemoiGenius
//
//  Created by Omar Carrassi on 06/12/18.
//  Copyright © 2018 Omar Carrassi. All rights reserved.
//

extension Dictionary {
    func merged(another: [Key: Value]) -> Dictionary {
        var result: [Key: Value] = [:]
        for (key, value) in self {
            result[key] = value
        }
        for (key, value) in another {
            result[key] = value
        }
        return result
    }
}
