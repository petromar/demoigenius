//
//  UserHeaderCollectionViewCell.swift
//  DemoiGenius
//
//  Created by Omar Carrassi on 12/12/18.
//  Copyright © 2018 Omar Carrassi. All rights reserved.
//

import UIKit

class UserHeaderCollectionViewCell: UICollectionViewCell {
    
    // MARK: - OUTLETS
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    
    
    // MARK: - LIFECYCLE METHODS

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: - RENDERING METHODS
    
    override func draw(_ rect: CGRect) {
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width / 2
        avatarImageView.layer.borderColor = UIColor.darkGray.cgColor
    }

}
