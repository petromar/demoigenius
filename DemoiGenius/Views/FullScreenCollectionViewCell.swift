//
//  FullScreenCollectionViewCell.swift
//  DemoiGenius
//
//  Created by Omar Carrassi on 13/12/18.
//  Copyright © 2018 Omar Carrassi. All rights reserved.
//

import UIKit
import AlamofireImage

class FullScreenCollectionViewCell: UICollectionViewCell, UIScrollViewDelegate {
    
    // MARK: - OUTLETS
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    
    
    // MARK: - PROPERTIES
    
    var dTapGR: UITapGestureRecognizer!
    var topInset: CGFloat = 0 {
        didSet {
            centerIfNeeded()
        }
    }
    

    // MARK: - LIFECYCLE METHODS
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Add gesture recognizer to zoom image
        dTapGR = UITapGestureRecognizer(target: self, action: #selector(doubleTap(gr:)))
        dTapGR.numberOfTapsRequired = 2
        addGestureRecognizer(dTapGR)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.likesLabel.text = ""
        self.commentsLabel.text = ""
        self.scrollView.delegate = self
        self.scrollView.contentMode = .center
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        scrollView.frame = bounds
        
        // Adjust imageView size and position based on scrollview and image dimensions
        let size: CGSize
        if let image = imageView.image {
            let containerSize = CGSize(width: bounds.width, height: bounds.height - topInset)
            if containerSize.width / containerSize.height < image.size.width / image.size.height {
                size = CGSize(width: containerSize.width, height: containerSize.width * image.size.height / image.size.width )
            } else {
                size = CGSize(width: containerSize.height * image.size.width / image.size.height, height: containerSize.height )
            }
        } else {
            size = CGSize(width: bounds.width, height: bounds.width)
        }
        
        imageView.frame = CGRect(origin: .zero, size: size)
        scrollView.contentSize = size
        
        // Center image in scrollview
        centerIfNeeded()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        scrollView.setZoomScale(1, animated: false)
        
        imageView.af_cancelImageRequest()
        imageView.layer.removeAllAnimations()
        imageView.image = nil

    }
    
    
    // MARK: - GESTURE METHODS
    
    @objc func doubleTap(gr: UITapGestureRecognizer)
    {
        // Zoom imageview in/out on double tap
        if scrollView.zoomScale == 1 {
            scrollView.zoom(to: zoomRectForScale(scale: scrollView.maximumZoomScale, center: gr.location(in: gr.view)), animated: true)
        } else {
            scrollView.setZoomScale(1, animated: true)
        }
    }
    
    
    // MARK: - SCROLLVIEWDELEGATE METHODS
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        centerIfNeeded()
    }
    
    
    // MARK: - ACTION METHODS
    
    func setLabelsVisibility(to visible: Bool)
    {
        likesLabel.isHidden = !visible
        commentsLabel.isHidden = !visible
    }
    
    
    // MARK: - UTILS
    
    func configureCell(withData media: Media)
    {
        // Set ImageView
        UIView.animate(withDuration: 0.5) {
            self.imageView.af_setImage(withURL: media.images.standard_resolution.url)
        }
        imageView.isOpaque = true
        
        // Set Likes label
        likesLabel.text = "\(media.likes.count) " + NSLocalizedString("likes.label", comment: "")
        
        // Set comments label
        commentsLabel.text = "\(media.comments.count) " + NSLocalizedString("comments.label", comment: "")
    }

    
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect
    {
        // Calculate new image sizes accordingly to scrollview maximum scale
        var zoomRect = CGRect.zero
        zoomRect.size.height = imageView.frame.size.height / scale
        zoomRect.size.width  = imageView.frame.size.width  / scale
        
        // Calculate new image center position
        let newCenter = imageView.convert(center, from: scrollView)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        
        return zoomRect
    }
    
    func centerIfNeeded() {
        // Set base inset
        var inset = UIEdgeInsets(top: topInset, left: 0, bottom: 0, right: 0)
        
        // Calculate vertical insets to center scrollview subviews
        if scrollView.contentSize.height < scrollView.bounds.height - topInset {
            let insetV = (scrollView.bounds.height - topInset - scrollView.contentSize.height)/2
            inset.top += insetV
            inset.bottom = insetV
        }
        
        // Calculate horizontal insets to center scrollview subviews
        if scrollView.contentSize.width < scrollView.bounds.width {
            let insetV = (scrollView.bounds.width - scrollView.contentSize.width)/2
            inset.left = insetV
            inset.right = insetV
        }
        
        scrollView.contentInset = inset
    }

}
