//
//  LabeledCollectionViewCell.swift
//  DemoiGenius
//
//  Created by Omar Carrassi on 08/12/2018.
//  Copyright © 2018 Omar Carrassi. All rights reserved.
//

import UIKit

class LabeledCollectionViewCell: UICollectionViewCell {
    
    // MARK: - OUTLETS
    
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var likesImageView: UIImageView!
    @IBOutlet weak var commentsImageView: UIImageView!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    

    // MARK: - LIFE CYCLE METHODS
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Setting default texts
        likesLabel.text = "0"
        commentsLabel.text = "0"
    }

}
