//
//  Instagram.swift
//  DemoiGenius
//
//  Created by Omar Carrassi on 08/12/18.
//  Copyright © 2018 Omar Carrassi. All rights reserved.
//

import Foundation

public struct Instagram: Decodable {
    
    let baseURL = "https://api.instagram.com"
    var authURL: String { return baseURL + "/oauth/authorize/" }
    var accessTokenURL: String { return baseURL + "/oauth/access_token/" }
    var currentUserApiURL: String { return baseURL + "/v1/users/self/" }
    var mediaApiURL: String { return baseURL + "/v1/users/self/media/recent" }
    
    let clientID: String?
    let clientSecret: String?
    let redirectURL: String?
    
    let scope = "public_content+likes+comments"
    let grantType = "authorization_code"
    
    let authCodeParamName = "code"
    
    let authErrorParamName = "error"
    let authErrorReasonParamName = "error_reason"
    let authErrorDescParamName = "error_description"
    
    enum error: Error {
        /// Error 400 on login.
        case badRequest
        
        // Empty or invalid AuthCode
        case emptyAuthCode
        
        // Empty or invalid AccessToken
        case authException
        
        // Invalid API request
        case invalidRequest
        
        // Invalid API request
        case invalidResponse
        
        // Keychain error
        case keychainError(code: OSStatus)
    }
    
    private enum CodingKeys: String, CodingKey {
        case clientID = "ClientID", clientSecret = "ClientSecret", redirectURL = "RedirectURI"
    }
    
    // MARK: - LIFE CYCLE METHODS
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        clientID = try container.decode(String.self, forKey: .clientID)
        clientSecret = try container.decode(String.self, forKey: .clientSecret)
        redirectURL = try container.decode(String.self, forKey: .redirectURL)
    }
}
