//
//  MainViewController.swift
//  DemoiGenius
//
//  Created by Omar Carrassi on 04/12/2018.
//  Copyright © 2018 Omar Carrassi. All rights reserved.
//

import UIKit
import IGListKit
import Hero

class MainViewController: UIViewController, ListAdapterDataSource, GridSectionControllerDelegate {
    
    // MARK: - OUTLETS
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    // MARK: - PROPERTIES
    
    var userManager: UserModelController!
    
    private lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }()
    private let collectionViewSectionsItems: [CollectionSectionItem] = [CollectionSectionItem()]
    
    
    // MARK: - LIFE CYCLE METHODS

    override func viewDidLoad()
    {
        super.viewDidLoad()
                
        // Add observer to network status notifications
        NotificationCenter.default.addObserver(self, selector: #selector(loadUserData), name: .networkReachable, object: nil)
        
        // Add collectionview to the view objects
        contentView.addSubview(collectionView)
        adapter.collectionView = collectionView
        adapter.dataSource = self
        
        toggleViewContent(showingUserPosts: userManager.userLoggedIn)
    }
    
    @objc private func loadUserData()
    {
        // Get user's data from storage or API
        if let user = userManager.currentUser {
            collectionViewSectionsItems.first?.user = user
        } else {
            userManager.fetchUserProfile() { (success, error) in
                if success == true {
                    self.collectionViewSectionsItems.first?.user = self.userManager.currentUser
                }
            }
        }
        
        userManager.fetchUserMedia() { (success, error, data) in
            if success == true {
                
                // Update only if new data found
                guard data.count > 0 else { return }
                
                // Cast result data to original type
                guard let mediaItemsArray = data as? [Media] else { return }
                
                // Set new items to show
                self.collectionViewSectionsItems.first?.items = mediaItemsArray
                
                // Reload collectionview datasource
                self.adapter.reloadObjects(self.collectionViewSectionsItems)
            }
        }
    }
    
    
    // MARK: - LISTADAPTERDATASOURCE METHODS
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable]
    {
        return collectionViewSectionsItems
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController
    {
        let gridSectionController = GridSectionController()
        gridSectionController.delegate = self
        
        return gridSectionController
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView?
    {
        return nil
    }
    
    
    // MARK: - GRIDSECTIONCONTROLLER DELEGATE METHODS
    
    func collectionItemSelected(in sectionController: GridSectionController, at index: Int)
    {
        if let fullScreenController = self.storyboard?.instantiateViewController(withIdentifier: "FullScreenViewController") as? FullScreenViewController {
            fullScreenController.collectionViewSectionsItems = self.collectionViewSectionsItems
            fullScreenController.visibleItemIndex = index
            DispatchQueue.main.async {
                self.present(fullScreenController, animated: true, completion: nil)
            }
        }
    }
    
    
    // MARK: - ACTIONS METHODS
    
    
    @IBAction func logoutButtonPressed(_ sender: UIBarButtonItem)
    {
        if userManager.logUserOut() == true {
            toggleViewContent(showingUserPosts: false)
        }
    }
    
    
    // MARK: - UTILS
    
    private func toggleViewContent(showingUserPosts: Bool)
    {
        logoImageView.isHidden = showingUserPosts
        loginButton.isHidden = showingUserPosts
        contentView.isHidden = !showingUserPosts
        
        if showingUserPosts == true {
            loadUserData()
        }
    }
    

    // MARK: - NAVIGATION

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "ShowAuthModal" {
            
            if let authViewController = segue.destination as? AuthViewController {
                authViewController.userManager = userManager
            }
            
        }
    }
    
    @IBAction func unwindAuthViewController(withSegue: UIStoryboardSegue)
    {
        toggleViewContent(showingUserPosts: userManager.userLoggedIn)
    }

}
