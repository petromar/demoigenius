//
//  FullScreenSectionController.swift
//  DemoiGenius
//
//  Created by Omar Carrassi on 13/12/18.
//  Copyright © 2018 Omar Carrassi. All rights reserved.
//

import IGListKit
import UIKit

final class FullScreenSectionController: ListSectionController {
    
    // MARK: - PROPERTIES
    
    private var object: CollectionSectionItem?
    
    
    // MARK: - LIFE CYCLE METHODS
    
    override required init() {
        super.init()
        
        self.minimumInteritemSpacing = 0
        self.minimumLineSpacing = 0
    }
    
    
    // MARK: - LISTSECTIONCONTROLLER DATASOURCE METHODS
    
    override func numberOfItems() -> Int
    {
        return object?.items.count ?? 0
    }
    
    override func sizeForItem(at index: Int) -> CGSize
    {
        let width = collectionContext?.containerSize.width ?? 0
        let height = collectionContext?.containerSize.height ?? 0
        return CGSize(width: width, height: height)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell
    {
        guard let cell = collectionContext?.dequeueReusableCell(withNibName: "FullScreenCollectionViewCell",
                                                                bundle: nil,
                                                                for: self,
                                                                at: index) as? FullScreenCollectionViewCell else {
            fatalError()
        }
        
        guard let currentMedia = object?.items[index] else { return cell }
        
        cell.imageView.isOpaque = true
        if #available(iOS 11.0, *) {
            cell.topInset = (self.viewController?.view.safeAreaInsets.top)!
        } else {
            cell.topInset = (self.viewController?.topLayoutGuide.length)!
        }
        
        cell.configureCell(withData: currentMedia)
        
        // Set animation related parameters
        cell.imageView.hero.isEnabled = true
        cell.imageView.hero.id = "image_\(index)"
        cell.imageView.hero.modifiers = [.position(CGPoint(x:(self.viewController?.view.bounds.width)!/2,
                                                           y:(self.viewController?.view.bounds.height)! + (self.viewController?.view.bounds.width)!/2)),
                                         .scale(0.6),
                                         .fade]
        cell.likesLabel.hero.isEnabled = true
        cell.likesLabel.hero.id = "likes_\(index)"
        cell.commentsLabel.hero.isEnabled = true
        cell.commentsLabel.hero.id = "comments_\(index)"
        
        return cell
    }
    
    override func didUpdate(to object: Any)
    {
        self.object = object as? CollectionSectionItem
    }
}
