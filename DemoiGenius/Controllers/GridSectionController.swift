//
//  GridSectionController.swift
//  DemoiGenius
//
//  Created by Omar Carrassi on 08/12/2018.
//  Copyright © 2018 Omar Carrassi. All rights reserved.
//

import IGListKit
import UIKit
import AlamofireImage

protocol GridSectionControllerDelegate: class {
    func collectionItemSelected(in sectionController: GridSectionController, at index: Int)
}

final class GridSectionController: ListSectionController, ListSupplementaryViewSource {
    
    // MARK: - PROPERTIES
    
    private var object: CollectionSectionItem?
    private let imageDownloader = ImageDownloader(configuration: ImageDownloader.defaultURLSessionConfiguration(),
                                                  downloadPrioritization: .fifo,
                                                  maximumActiveDownloads: 4,
                                                  imageCache: AutoPurgingImageCache())
    weak var delegate: GridSectionControllerDelegate?
    
    
    // MARK: - LIFE CYCLE METHODS
    
    override required init() {
        super.init()
        
        self.minimumInteritemSpacing = 0
        self.minimumLineSpacing = 1
        
        supplementaryViewSource = self
    }
    
    
    // MARK: - LISTSECTIONCONTROLLER DATASOURCE METHODS
    
    override func numberOfItems() -> Int
    {
        return object?.items.count ?? 0
    }
    
    override func sizeForItem(at index: Int) -> CGSize
    {
        let width = collectionContext?.containerSize.width ?? 0
        let itemSize = floor(width / 4)
        return CGSize(width: itemSize, height: itemSize)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell
    {
        guard let cell = collectionContext?.dequeueReusableCell(withNibName: "LabeledCollectionViewCell",
                                                                bundle: nil,
                                                                for: self,
                                                                at: index) as? LabeledCollectionViewCell else {
            fatalError()
        }
        
        guard let currentMedia = object?.items[index] else { return cell }
        
        // Set Thumb ImageView
        imageDownloader.download(URLRequest(url: currentMedia.images.thumbnail.url)) { (response) in
            if let image = response.result.value {
                cell.thumbImageView.image = image
            } else {
                cell.thumbImageView.image = UIImage(named: "image_placeholder")
            }
        }
//        cell.thumbImageView.af_setImage(withURL: currentMedia.images.thumbnail.url, placeholderImage: UIImage(named: "image_placeholder"))
        
        // Set Likes label and image
        cell.likesLabel.text = "\(currentMedia.likes.count)"
            
        if currentMedia.likes.count == 0 {
            cell.likesImageView.image = UIImage(named: "empty_heart")
        } else {
            cell.likesImageView.image = UIImage(named: "filled_heart")
        }
        
        // Set comments label and image
        cell.commentsLabel.text = "\(currentMedia.comments.count)"
        
        if currentMedia.comments.count == 0 {
            cell.commentsImageView.image = UIImage(named: "empty_comments")
        } else {
            cell.commentsImageView.image = UIImage(named: "filled_comments")
        }
        
        return cell
    }
    
    override func didUpdate(to object: Any)
    {
        self.object = object as? CollectionSectionItem
    }
    
    
    // MARK: LISTSUPPLEMENTARYVIEW DATASOURCE METHODS
    
    func supportedElementKinds() -> [String]
    {
        return [UICollectionView.elementKindSectionHeader]
    }
    
    func viewForSupplementaryElement(ofKind elementKind: String, at index: Int) -> UICollectionReusableView
    {
        return userHeaderView(atIndex: index)
    }
    
    func sizeForSupplementaryView(ofKind elementKind: String, at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: 100)
    }
    
    private func userHeaderView(atIndex index: Int) -> UICollectionReusableView
    {
        guard let view = collectionContext?.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                                             for: self,
                                                                             nibName: "UserHeaderCollectionViewCell",
                                                                             bundle: nil,
                                                                             at: index) as? UserHeaderCollectionViewCell else {
            fatalError()
        }
        
        guard let currentUser = object?.user else { return view }
        
        // Set Avatar ImageView
        let filter = AspectScaledToFillSizeCircleFilter(size: CGSize(width: 100.0, height: 100.0))
        imageDownloader.download(URLRequest(url: URL(string: currentUser.profilePicUrl)!), filter: filter) { (response) in
            if let image = response.result.value {
                view.avatarImageView.image = image
            }
        }
        
        // Set Full Name Label
        view.fullNameLabel.text = currentUser.fullName
        
        return view
    }
    
    
    // MARK: - DELEGATE METHODS
    
    override func didSelectItem(at index: Int) {
        delegate?.collectionItemSelected(in: self, at: index)
    }
}
