//
//  FullScreenViewController.swift
//  DemoiGenius
//
//  Created by Omar Carrassi on 12/12/2018.
//  Copyright © 2018 Omar Carrassi. All rights reserved.
//

import UIKit
import IGListKit
import AlamofireImage
import Hero

class FullScreenViewController: UIViewController, ListAdapterDataSource, UIGestureRecognizerDelegate {
    
    // MARK: - OUTLETS
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var closeButton: UIButton!
    
    
    // MARK: - PROPERTIES
    
    private lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }()
    var collectionViewSectionsItems: [CollectionSectionItem] = [CollectionSectionItem()]
    var visibleItemIndex: Int = 0
    var panGR = UIPanGestureRecognizer()
    
    
    // MARK: - LIFE CYCLE METHODS

    override func viewDidLoad() {
        super.viewDidLoad()
        
        automaticallyAdjustsScrollViewInsets = false
        preferredContentSize = CGSize(width: view.bounds.width, height: view.bounds.width)

        // Add collectionview to the view objects
        adapter.collectionView = collectionView
        adapter.dataSource = self
        
        // Scroll to selected image
        view.layoutIfNeeded()
        collectionView.scrollToItem(at: IndexPath(row: visibleItemIndex, section: 0),
                                    at: .centeredHorizontally,
                                    animated: false)
                
        // Add pan gesture recognizer to collectionview
        panGR.addTarget(self, action: #selector(pan))
        panGR.delegate = self
        collectionView?.addGestureRecognizer(panGR)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        for cell in (collectionView!.visibleCells as? [FullScreenCollectionViewCell])! {
            if #available(iOS 11.0, *) {
                cell.topInset = self.view.safeAreaInsets.top
            } else {
                cell.topInset = self.topLayoutGuide.length
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        collectionViewSectionsItems = [CollectionSectionItem()]
        visibleItemIndex = 0
    }
    
    
    // MARK: - LISTADAPTERDATASOURCE METHODS
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable]
    {
        return collectionViewSectionsItems
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController
    {
        return FullScreenSectionController()
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView?
    {
        return nil
    }
    
    
    // MARK: - ACTION METHODS
    
    @IBAction func hideViewController(_ sender: Any)
    {
        dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - GESTURE RECOGNIZER METHODS
    
    @objc func pan() {
        let translation = panGR.translation(in: nil)
        let progress = translation.y / 2 / collectionView!.bounds.height
        
        // Handle collectionView cell animation accordingly to user touch
        switch panGR.state {
        case .began:
            hero.dismissViewController()
        case .changed:
            Hero.shared.update(progress)
            if let cell = collectionView?.visibleCells[0]  as? FullScreenCollectionViewCell {
                cell.setLabelsVisibility(to: false)
                let currentPos = CGPoint(x: translation.x + view.center.x, y: translation.y + view.center.y)
                Hero.shared.apply(modifiers: [.position(currentPos)], to: cell.imageView)
            }
        default:
            if progress + panGR.velocity(in: nil).y / collectionView!.bounds.height > 0.3 {
                Hero.shared.finish()
            } else {
                if let cell = collectionView?.visibleCells[0]  as? FullScreenCollectionViewCell {
                    cell.setLabelsVisibility(to: true)
                }
                Hero.shared.cancel()
            }
        }
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        // Stop gesture action if scrollview zoomed in
        if let cell = collectionView?.visibleCells[0] as? FullScreenCollectionViewCell,
            cell.scrollView.zoomScale == 1 {
            let v = panGR.velocity(in: nil)
            return v.y > abs(v.x)
        }
        return false
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
