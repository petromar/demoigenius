//
//  AuthViewController.swift
//  DemoiGenius
//
//  Created by Omar Carrassi on 02/12/2018.
//  Copyright © 2018 Omar Carrassi. All rights reserved.
//

import UIKit
import WebKit

class AuthViewController: UIViewController, WKNavigationDelegate {
    
    // MARK: - OUTLETS
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    
    // MARK: - PROPERTIES
    
    private let _instagram: Instagram
    var userManager: UserModelController!
    var authWebView: WKWebView!
    var authSucceded: Bool = false
    
    
    // MARK: - LIFE CYCLE METHODS
    
    required init?(coder aDecoder: NSCoder) {
        
        // Instantiate Instagram object using plist data
        do {
            if  let instagramPlistDict = Bundle.main.object(forInfoDictionaryKey: "Instagram") {
                let instagramPlistData = try JSONSerialization.data(withJSONObject: instagramPlistDict)
                _instagram = try JSONDecoder().decode(Instagram.self, from: instagramPlistData)
            } else {
                fatalError("Missing Instagram config in Info.plist")
            }
        } catch _ {
            fatalError("Missing Instagram config in Info.plist")
        }
        
        super.init(coder: aDecoder)
    }
    
    /*
     *  Setting up the WKWebView programmatically
     *  to avoid iOS < 11 compile error (initWithCoder bug)
     */
    override func loadView()
    {
        super.loadView()
        
        let webConfiguration = WKWebViewConfiguration()
        let webViewFrame = CGRect(x: 0,
                                  y: navigationBar.frame.origin.y + navigationBar.frame.size.height,
                                  width: view.frame.width,
                                  height: view.frame.height)

        authWebView = WKWebView(frame: webViewFrame, configuration: webConfiguration)
        authWebView.navigationDelegate = self

        view.addSubview(authWebView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        authWebView.frame = CGRect(x: 0,
                                   y: navigationBar.frame.origin.y + navigationBar.frame.size.height,
                                   width: view.frame.width,
                                   height: view.frame.height)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Start Instagram auth flow to get newely created auth code and access token
        performAuthRequest()
    }
    
    
    // MARK: - AUTH REQUEST METHODS
    
    private func performAuthRequest()
    {
        let authUrl = String(format: _instagram.authURL + "?client_id=%@&redirect_uri=%@&response_type=code&scope=%@&DEBUG=True",
                             arguments: [_instagram.clientID!, _instagram.redirectURL!, _instagram.scope])
        let authRequest = URLRequest(url: URL(string: authUrl)!)
        
        authWebView.load(authRequest)
    }
    
    private func checkForRedirectUrlIn(request: URLRequest) -> Bool
    {
        // Grab the current request URL
        guard let requestUrl = request.url?.absoluteString else { return true }
        
        // Check the request type searching for Instagram's redirect URL
        if requestUrl.hasPrefix(_instagram.redirectURL!) {
            
            // Stop the auth process if userManager is nil
            guard userManager != nil else { return false }
            
            // Stop the request if no parameters given
            guard let urlQueryItems = URLComponents(string: requestUrl)?.queryItems else { return false }
            
            // Show any error and unwind view controller
            if let authError = checkForAuthErrorIn(queryItems: urlQueryItems) {
                self.authSucceded = false
                self.performSegue(withIdentifier: "UnwindAuthViewController", sender: nil)
                #warning("Complete showing error message")
                return false
            }
            
            // Grab the auth code and start access token request
            if let authCode = checkForAuthCodeIn(queryItems: urlQueryItems) {
                userManager?.logUserIn(withAuthCode: authCode) { (success, error) in
                    self.authSucceded = success
                    
                    if let error = error {
                        switch error {
                        case Instagram.error.invalidRequest:
                            #warning("Complete showing error message")
                        case Instagram.error.invalidResponse:
                            #warning("Complete showing error message")
                        case Instagram.error.authException:
                            #warning("Complete showing error message")
                        case Instagram.error.keychainError:
                            #warning("Complete showing error message")
                        default:
                            break
                        }
                    }
                    
                    self.performSegue(withIdentifier: "UnwindAuthViewController", sender: nil)
                }
                return false
            }
        }
        
        // Let non Instagram's requests to load
        return true
    }
    
    
    // MARK: - WK DELEGATE METHODS
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void)
    {
        // Check for Instagram's redirect request
        if checkForRedirectUrlIn(request: navigationAction.request) {
            decisionHandler(.allow)
        } else {
            decisionHandler(.cancel)
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error)
    {
        print(error.localizedDescription)
        #warning("Complete showing error message")
    }
    
    
    // MARK: - UTILS
    
    /*
     *  Parses the given array searching for error parameters in query string
     */
    private func checkForAuthErrorIn(queryItems: [URLQueryItem]) -> (String, String, String)?
    {
        var error: String = ""
        var errorReason: String = ""
        var errorDesc: String = ""
        
        for queryItem in queryItems {
            
            guard let value = queryItem.value else { continue }
            
            switch queryItem.name {
            case _instagram.authErrorParamName:
                error = value
            case _instagram.authErrorReasonParamName:
                errorReason = value
            case _instagram.authErrorDescParamName:
                errorDesc = value
            default:
                break
            }
        }
        
        if error != "" && errorReason != "" && errorDesc != "" {
            return (error, errorReason, errorDesc)
        } else {
            return nil
        }
        
    }
    
    /*
     *  Parses the given array searching for AuthCode parameter in query string
     */
    private func checkForAuthCodeIn(queryItems: [URLQueryItem]) -> String?
    {
        guard let code = queryItems.filter({ (item) in
            item.name == _instagram.authCodeParamName
        }).first?.value else { return nil }
        
        return code
    }
    
    
    // MARK: - ACTIONS METHODS
    
    
    @IBAction func cancelButtonTapped(_ sender: Any)
    {
        authSucceded = false
        performSegue(withIdentifier: "UnwindAuthViewController", sender: sender)
    }
    
    
    // MARK: - Navigation

//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//         Get the new view controller using segue.destination.
//         Pass the selected object to the new view controller.
//    }

}
